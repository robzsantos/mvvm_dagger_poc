package com.example.mvvmdaggerpoc.ui.main

import com.example.mvvmdaggerpoc.MvvmDaggerPocRepository
import com.example.mvvmdaggerpoc.MvvmDaggerPocViewModel
import com.example.mvvmdaggerpoc.rxutils.SchedulerProvider
import com.example.mvvmdaggerpoc.webservice.IpAddressResponse
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
 * Unit test for [MvvmDaggerPocViewModel].
 */
class MvvmDaggerPocActivityViewModelTest {

    @Mock
    private lateinit var mockRepository: MvvmDaggerPocRepository

    private val schedulerProvider =
        SchedulerProvider(Schedulers.trampoline(), Schedulers.trampoline())

    private lateinit var mvvmDaggerPocActivityViewModel: MvvmDaggerPocViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mvvmDaggerPocActivityViewModel = MvvmDaggerPocViewModel(mockRepository, schedulerProvider)
    }

    @Test
    fun showDataFromApi() {
        whenever(mockRepository.getIpData()).thenReturn(Single.just(IpAddressResponse("20.0.0.0")))

        val testObserver = TestObserver<IpAddressResponse>()

        mvvmDaggerPocActivityViewModel.getSingleIpAddressFromApi()
            .subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertValue { ipAddress -> ipAddress.ip == "20.0.0.0" }
    }
}