package com.example.mvvmdaggerpoc

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.example.mvvmdaggerpoc.base.BaseActivity
import com.example.mvvmdaggerpoc.databinding.ActivityMvvmDaggerPocBinding
import com.example.mvvmdaggerpoc.rxutils.ViewState
import kotlinx.android.synthetic.main.activity_mvvm_dagger_poc.*
import javax.inject.Inject

class MvvmDaggerPocActivity : BaseActivity<MvvmDaggerPocViewModel>() {

    @Inject
    lateinit var mvvmDaggerPocViewModel: MvvmDaggerPocViewModel

    private lateinit var binding: ActivityMvvmDaggerPocBinding

    override fun getViewModel(): MvvmDaggerPocViewModel {
        return mvvmDaggerPocViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_mvvm_dagger_poc)
        binding.viewModel = mvvmDaggerPocViewModel
        binding.lifecycleOwner = this

        getIpAddress()

        getGithubInfo()
    }

    override fun onDestroy() {
        super.onDestroy()
        mvvmDaggerPocViewModel.clear()
    }

    private fun getIpAddress() {
        mvvmDaggerPocViewModel.getIpAddress()

        mvvmDaggerPocViewModel.showIpAddressLiveData.observe(this, Observer { it ->
            textview_ip_address_driver_poc.text = it.ip
        })
    }

    private fun getGithubInfo() {
        mvvmDaggerPocViewModel.showGithubProjectState.observe(this, Observer { state ->

            when (state) {
                is ViewState.Success -> {
                    progressbar_driver_poc.visibility = View.GONE

                    textview_name_project_driver_poc.text = state.data.name
                    textview_info_project_driver_poc.text =
                        getString(
                            R.string.activity_driver_poc_info_github_project,
                            state.data.rating,
                            state.data.forks
                        )
                    textview_description_project_driver_poc.text = state.data.description
                    textview_owner_project_driver_poc.text = state.data.ownerName
                }
                is ViewState.Loading -> {
                    progressbar_driver_poc.visibility = View.VISIBLE
                }
                is ViewState.Failed -> {
                    progressbar_driver_poc.visibility = View.GONE
                    textview_name_project_driver_poc.text = state.throwable.message
                }
            }

        })
    }
}
