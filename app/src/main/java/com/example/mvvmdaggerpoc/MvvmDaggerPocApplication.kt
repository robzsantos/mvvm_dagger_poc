package com.example.mvvmdaggerpoc

import com.example.mvvmdaggerpoc.di.DaggerMvvmDaggerPocComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

/**
 * MVVM - Dagger - Data binding - Live Data - POC
 *
 * References:
 *
 * https://medium.com/@princessdharmy/dagger-2-with-mvvm-simplified-3215b3cf4c96
 * https://medium.com/@rahul.singh/clean-architecture-kotlin-dagger-2-rxjava-mvvm-and-unit-testing-dc05dcdf3ce6
 * https://medium.com/android-dev-br/modulariza%C3%A7%C3%A3o-android-parte-3-22622ae7d162
 */
class MvvmDaggerPocApplication : DaggerApplication() {

    private val instance: MvvmDaggerPocApplication? = null

    @Synchronized
    fun getInstance(): MvvmDaggerPocApplication? {
        return instance
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerMvvmDaggerPocComponent.builder()
            .create(this)
    }

}