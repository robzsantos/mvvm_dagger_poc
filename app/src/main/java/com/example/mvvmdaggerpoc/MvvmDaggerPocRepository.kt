package com.example.mvvmdaggerpoc

import com.example.mvvmdaggerpoc.webservice.GithubApiService
import com.example.mvvmdaggerpoc.webservice.IpAddressResponse
import com.example.mvvmdaggerpoc.webservice.IpApiService
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MvvmDaggerPocRepository @Inject constructor(
    private val ipApiService: IpApiService, private val githubApiService: GithubApiService
) {

    fun getIpData(): Single<IpAddressResponse> = ipApiService.getJsonResponse()

    fun getGithubList(page: Int) = githubApiService.getList(language = "language:kotlin", sort = "stars", page = page)

}