package com.example.mvvmdaggerpoc.di.modules

import com.example.mvvmdaggerpoc.BuildConfig
import com.example.mvvmdaggerpoc.di.qualifiers.GithubApi
import com.example.mvvmdaggerpoc.di.qualifiers.IpApi
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RetrofitModule {

    private fun basicBuilder(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    @Provides
    @Singleton
    @IpApi
    fun providesIpApiRetrofit(client: OkHttpClient, gson: Gson): Retrofit {
        return basicBuilder(gson)
            .baseUrl(BuildConfig.IP_API)
            .client(client)
            .build()
    }

    @Provides
    @Singleton
    @GithubApi
    fun providesGithubApiRetrofit(client: OkHttpClient, gson: Gson): Retrofit {
        return basicBuilder(gson)
            .baseUrl(BuildConfig.GITHUB_API)
            .client(client)
            .build()
    }

}