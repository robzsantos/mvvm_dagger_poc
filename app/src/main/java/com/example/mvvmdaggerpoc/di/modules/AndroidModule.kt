package com.example.mvvmdaggerpoc.di.modules

import android.content.Context
import com.example.mvvmdaggerpoc.MvvmDaggerPocApplication
import com.example.mvvmdaggerpoc.rxutils.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class AndroidModule {

    @Singleton
    @Provides
    fun provideContext(application: MvvmDaggerPocApplication): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideSchedulerProvider() =
        SchedulerProvider(
            Schedulers.io(),
            AndroidSchedulers.mainThread()
        )

}