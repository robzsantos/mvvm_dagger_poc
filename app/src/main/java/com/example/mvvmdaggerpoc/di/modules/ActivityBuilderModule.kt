package com.example.mvvmdaggerpoc.di.modules

import com.example.mvvmdaggerpoc.MvvmDaggerPocActivity
import com.example.mvvmdaggerpoc.di.modules.views.MvvmDaggerPocActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = [MvvmDaggerPocActivityModule::class])
    internal abstract fun contributeMainActivity(): MvvmDaggerPocActivity

}