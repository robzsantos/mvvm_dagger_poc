package com.example.mvvmdaggerpoc.di.modules

import android.content.Context
import com.example.mvvmdaggerpoc.di.LoggerInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import java.io.File
import javax.inject.Singleton

@Module
class OkHttpClientModule {

    @Provides
    @Singleton
    fun providesDefaultOkHttpClient(context: Context): OkHttpClient {
        val builder = OkHttpClient.Builder()

        builder.cache(
            Cache(
                File(
                    context.cacheDir,
                    CACHE_FOLDER
                ), CACHE_MAX_SIZE.toLong()
            )
        )

        builder.addNetworkInterceptor(LoggerInterceptor())

        return builder.build()
    }

    companion object {

        private const val CACHE_FOLDER = "http_cache"

        private const val CACHE_MAX_SIZE = 1024 * 1024 * 10 //10MB
    }
}