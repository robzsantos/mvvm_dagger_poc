package com.example.mvvmdaggerpoc.di

import com.example.mvvmdaggerpoc.MvvmDaggerPocApplication
import com.example.mvvmdaggerpoc.di.modules.*
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidModule::class, AndroidSupportInjectionModule::class, OkHttpClientModule::class,
        RetrofitModule::class, ApisModule::class, GsonModule::class, ActivityBuilderModule::class]
)

interface MvvmDaggerPocComponent : AndroidInjector<MvvmDaggerPocApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MvvmDaggerPocApplication>()

}