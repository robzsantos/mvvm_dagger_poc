package com.example.mvvmdaggerpoc.di.modules.views

import com.example.mvvmdaggerpoc.MvvmDaggerPocRepository
import com.example.mvvmdaggerpoc.MvvmDaggerPocViewModel
import com.example.mvvmdaggerpoc.rxutils.SchedulerProvider
import dagger.Module
import dagger.Provides


@Module
class MvvmDaggerPocActivityModule {

    @Provides
    internal fun provideMvvmDaggerPocActivityViewModel(
        repository: MvvmDaggerPocRepository
        , schedulerProvider: SchedulerProvider
    ) = MvvmDaggerPocViewModel(repository, schedulerProvider)

}