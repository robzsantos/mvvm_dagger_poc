package com.example.mvvmdaggerpoc.di.modules

import com.example.mvvmdaggerpoc.di.qualifiers.GithubApi
import com.example.mvvmdaggerpoc.di.qualifiers.IpApi
import com.example.mvvmdaggerpoc.webservice.GithubApiService
import com.example.mvvmdaggerpoc.webservice.IpApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApisModule {

    @Provides
    @Singleton
    fun providesIpApi(@IpApi retrofit: Retrofit): IpApiService =
        retrofit.create(IpApiService::class.java)

    @Provides
    @Singleton
    fun providesGithubApi(@GithubApi retrofit: Retrofit): GithubApiService =
        retrofit.create(GithubApiService::class.java)
}