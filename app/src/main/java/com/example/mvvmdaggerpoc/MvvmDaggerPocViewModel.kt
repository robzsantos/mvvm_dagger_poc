package com.example.mvvmdaggerpoc

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvmdaggerpoc.rxutils.SchedulerProvider
import com.example.mvvmdaggerpoc.rxutils.StateMachineSingle
import com.example.mvvmdaggerpoc.rxutils.ViewState
import com.example.mvvmdaggerpoc.uidata.GithubKotlinProject
import com.example.mvvmdaggerpoc.webservice.IpAddressResponse
import com.example.mvvmdaggerpoc.webservice.ProjectsListResponse
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject


class MvvmDaggerPocViewModel @Inject constructor(
    private val repository: MvvmDaggerPocRepository,
    private val schedulerProvider: SchedulerProvider
) :
    ViewModel() {

    private val disposables = CompositeDisposable()

    val showIpAddressLiveData = MutableLiveData<IpAddressResponse>()

    val showGithubProjectState = MutableLiveData<ViewState<GithubKotlinProject>>()

    fun getSingleIpAddressFromApi(): Single<IpAddressResponse> = repository.getIpData()
        .compose(schedulerProvider.getSchedulersForSingle())

    fun getSingleProjectsFromApi(): Single<ProjectsListResponse> = repository.getGithubList(1)
        .compose(schedulerProvider.getSchedulersForSingle())

    fun getIpAddress() {

        disposables += getSingleIpAddressFromApi()
            .subscribe({
                showIpAddressLiveData.postValue(it)
            },
                {
                    Log.d("MainActivityVM", it?.message)
                })

    }

    fun getProject() {
        showGithubProjectState.apply { value = ViewState.Loading }

        disposables += getSingleProjectsFromApi()
            .map { GithubKotlinProject.from(it.list.first()) }
            .compose(StateMachineSingle())
            .subscribe({
                showGithubProjectState.postValue(it)
            }) {}
    }

    fun clear() {
        disposables.clear()
        disposables.dispose()
    }
}