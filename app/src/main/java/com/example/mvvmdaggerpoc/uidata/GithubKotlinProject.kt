package com.example.mvvmdaggerpoc.uidata

import com.example.mvvmdaggerpoc.webservice.ProjectResponse
import java.io.Serializable

class GithubKotlinProject : Serializable {

    var name: String? = null

    var description: String? = null

    var rating: Long? = null

    var forks: Long? = null

    var ownerName: String? = null

    companion object {

        fun from(kotlinProject: ProjectResponse): GithubKotlinProject =
            GithubKotlinProject().apply {
                this.name = kotlinProject.name
                this.rating = kotlinProject.score
                this.forks = kotlinProject.forks
                this.description = kotlinProject.description
                this.ownerName = kotlinProject.owner.name
            }
    }
}