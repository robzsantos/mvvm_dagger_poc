package com.example.mvvmdaggerpoc.webservice

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApiService {

    @GET("search/repositories?")
    fun getList(
        @Query("q") language: String, @Query("sort") sort: String,
        @Query("page") page: Int
    ): Single<ProjectsListResponse>
}