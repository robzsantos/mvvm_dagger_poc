package com.example.mvvmdaggerpoc.webservice

import com.google.gson.annotations.SerializedName

data class ProjectsListResponse(

    @SerializedName("total_count")
    val total: Int,

    @SerializedName("items")
    val list: List<ProjectResponse>

)

data class ProjectResponse(

    @SerializedName("name")
    val name: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("stargazers_count")
    val score: Long,

    @SerializedName("forks_count")
    val forks: Long,

    @SerializedName("owner")
    val owner: OwnerResponse
)

data class OwnerResponse(

    @SerializedName("login")
    val name: String,

    @SerializedName("avatar_url")
    val avatar: String

)