package com.example.mvvmdaggerpoc.webservice

data class IpAddressResponse(val ip: String)