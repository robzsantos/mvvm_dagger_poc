package com.example.mvvmdaggerpoc.webservice

import io.reactivex.Single
import retrofit2.http.GET

interface IpApiService {

    @GET(".")
    fun getJsonResponse(): Single<IpAddressResponse>
}